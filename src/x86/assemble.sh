#!/bin/bash
###################################################################### 
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

which nasm >/dev/null || sudo apt install nasm

dir="../../bin/x86"
mkdir -p "$dir"

for a in *.asm
do
  name="${a%.*}"
  nasm -f elf64 $a -o "${dir}/${name}.o" &&
    ld ${dir}/${name}.o -o ${dir}/${name} &&
    echo "${dir}/${name} Created."
  
done
