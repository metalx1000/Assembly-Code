section .data
  text db "Hello World!", 10 ; String and 10 is newline char

section .text
  global _start

_start:

_printText:
  mov rax, 1      ;starting with 'r' means it's a register
  mov rdi, 1      ;1 in rdi is for stdout 
  mov rsi, text
  mov rdx, 14     ;14 is the length of the string 'text' we created
  syscall

  mov rax, 60     ;60 means exit
  mov rdi, 0      ;0 is our exit code.  0 means success
  syscall
