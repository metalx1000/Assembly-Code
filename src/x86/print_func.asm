section .data
  text1 db "Hello World! Please Visit", 10, 0 ;10 is newline, 0 is end of string
  ; with backticks, \n, \r, \t, etc are available
  text2 db `https://filmsbykris.com\n`,0 ; 0 is end of string

section .text
  global _start

_start:
  mov rax, text1
  call _print

  mov rax, text2
  call _print

  mov rax, 60     ;60 means exit
  mov rdi, 0      ;0 is our exit code.  0 means success
  syscall
  

_print:
  push rax
  mov rbx, 0

;loop through each char of string
_printLoop:
  inc rax
  inc rbx
  mov cl, [rax]
  cmp cl,0
  jne _printLoop

  mov rax, 1      ;starting with 'r' means it's a register
  mov rdi, 1      ;1 in rdi is for stdout 
  pop rsi
  mov rdx, rbx
  syscall

  ret
