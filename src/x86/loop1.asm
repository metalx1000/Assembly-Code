;This Code will loop forever printing "Hello World"
section .data
  text db "Hello World!", 10 ; String and 10 is newline char

section .text
  global _start

_start:
  mov rax, 1      ;starting with 'r' means it's a register
  mov rdi, 1      ;1 in rdi is for stdout 
  mov rsi, text
  mov rdx, 14     ;14 is the length of the string 'text' we created
  syscall
  jmp _start      ;jump back to _start, meaning loop forever
