;This is just like our other hello world examples
;except, we use backticks so we can use special chars
;such as \n and \t

section .data
  ; with backticks, \n, \r, \t, etc are available
  text db `Hello World!\nPlease Visit\nhttps://filmsbykris.com\n`
  text_size EQU $ - text ;  Here we are getting the length of our string

section .text
  global _start

_start:

_printText:
  mov rax, 1      ;starting with 'r' means it's a register
  mov rdi, 1      ;1 in rdi is for stdout 
  mov rsi, text
  mov rdx, text_size    ;text_size is the length of the string 'text' we created
  syscall

  mov rax, 60     ;60 means exit
  mov rdi, 0      ;0 is our exit code.  0 means success
  syscall
