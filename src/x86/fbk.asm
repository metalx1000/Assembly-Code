section .data
  text1 db "Hello World! "
  text2 db "https://filmsbykris.com"

section .text
  global _start
_start:

_printText2:
  mov rax, 1
  mov rdi, 1
  mov rsi, text1
  mov rdx, 14
  syscall

  mov rax, 1
  mov rdi, 1
  mov rsi, text2
  mov rdx, 14
  syscall

  mov rax, 60
  mov rdi, 0
  syscall
